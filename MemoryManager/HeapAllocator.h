#pragma once

#include <cstddef>

template<typename T>
class HeapAllocator
{
public:

	using value_type = T;

	HeapAllocator() noexcept {}
	template<class U> HeapAllocator(const HeapAllocator<U>&) noexcept {}

	inline T* allocate(std::size_t n)
	{
		return static_cast<T*>(malloc(n * sizeof(T)));
	}

	inline void deallocate(T* p, std::size_t n)
	{
		free(p);
	}
};
