#pragma once
#include <cstddef>
#include <vector>
#include "HeapAllocator.h"

#define POOL_SIZE 4096 //4KB

class MemControlBlockAllocator
{
public:
	void* Allocate(std::size_t numBytes);
	void Deallocate(void* p);

private:

	struct MemControlBlock
	{
		MemControlBlock(bool available, MemControlBlock* prev, MemControlBlock* next);

		void MergeWithNextBlock();

		bool available_;
		MemControlBlock* prev_;
		MemControlBlock* next_;
	};

	void* CreatePool(std::size_t numBytes);
	void* AllocateInPool(MemControlBlock* head, std::size_t numBytes);
	bool IsPoolFree(MemControlBlock* head);

	std::vector<MemControlBlock*, HeapAllocator<MemControlBlock*>> pools;
};
