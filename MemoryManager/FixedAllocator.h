#pragma once

#include <cstddef>
#include <vector>
#include "Chunk.h"
#include "HeapAllocator.h"

class FixedAllocator
{
public:
	FixedAllocator(std::size_t blockSize);
	void* Allocate();
	void Deallocate(void* p);
	std::size_t BlockSize() const;
	bool operator<(std::size_t rhs) const;
private:
	std::size_t blockSize_;
	unsigned char numBlocks_;
	typedef std::vector<Chunk, HeapAllocator<Chunk>> Chunks;
	Chunks chunks_;
	Chunk* allocChunk_;
	Chunk* deallocChunk_;

	Chunk* VicinityFind(void* p);
	void DoDeallocate(void* p);
};

