#pragma once
#include "MemoryManager.h"

#ifndef MM_DISABLE_OVERLOADS
_NODISCARD _Ret_notnull_ _Post_writable_byte_size_(bytes) _VCRT_ALLOCATOR
void* __cdecl operator new(size_t bytes)
{
	return MemoryManager::GetInstance()->Allocate(bytes);
}

_NODISCARD _Ret_notnull_ _Post_writable_byte_size_(bytes) _VCRT_ALLOCATOR
void* __cdecl operator new[](size_t bytes)
{
	return MemoryManager::GetInstance()->AllocateArray(bytes);
}

void operator delete(void* p, size_t size)
{
	MemoryManager::GetInstance()->Delete(p, size);
}

void operator delete[](void* p, size_t size)
{
	MemoryManager::GetInstance()->DeleteArray(p, size);
}
#endif //DISABLE_MM