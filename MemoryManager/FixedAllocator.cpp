#include <cassert>
#include "FixedAllocator.h"

FixedAllocator::FixedAllocator(std::size_t blockSize) :
	blockSize_(blockSize),
	allocChunk_(nullptr),
	deallocChunk_(nullptr)
{
	assert(blockSize > 0);

	std::size_t numBlocks = DEFAULT_CHUNK_SIZE / blockSize;
	if (numBlocks > UCHAR_MAX) numBlocks = UCHAR_MAX;
	else if (numBlocks == 0) numBlocks = 8 * blockSize;

	numBlocks_ = static_cast<unsigned char>(numBlocks);
	assert((unsigned)numBlocks_ == numBlocks);
}

void* FixedAllocator::Allocate()
{
	if (allocChunk_ == 0 || allocChunk_->blocksAvailable_ == 0)
	{
		// No available memory in this chunk
		// Try to find one
		Chunks::iterator i = chunks_.begin();
		for (;; ++i)
		{
			if (i == chunks_.end())
			{
				// All filled up-add a new chunk
				chunks_.reserve(chunks_.size() + 1);
				Chunk newChunk;
				newChunk.Init(blockSize_, numBlocks_);
				chunks_.push_back(newChunk);
				allocChunk_ = &chunks_.back();
				deallocChunk_ = &chunks_.back();
				break;
			}
			if (i->blocksAvailable_ > 0)
			{
				// Found a chunk
				allocChunk_ = &*i;
				break;
			}
		}
	}
	assert(allocChunk_ != 0);
	assert(allocChunk_->blocksAvailable_ > 0);
	return allocChunk_->Allocate(blockSize_);
}

void FixedAllocator::Deallocate(void* p)
{
	assert(!chunks_.empty());
	assert(&chunks_.front() <= allocChunk_);
	assert(&chunks_.back() >= deallocChunk_);

	deallocChunk_ = VicinityFind(p);
	assert(deallocChunk_);

	DoDeallocate(p);
}

// MY IMPLEMENTATION, WRONG BTW
//Chunk* FixedAllocator::VicinityFind(void* p)
//{
//	if (p == deallocChunk_)
//		return deallocChunk_;
//
//	Chunk* forward;
//	Chunk* backward;
//	int i = 0;
//	for (;;++i)
//	{
//		if (deallocChunk_ + i > &chunks_.back() && deallocChunk_ - i < &chunks_.front())
//			return nullptr;
//
//		if (deallocChunk_ + i <= &chunks_.back() && p == deallocChunk_ + i)
//			return deallocChunk_ + i;
//		if (deallocChunk_ - i >= &chunks_.front() && p == deallocChunk_ - i)
//			return deallocChunk_ - i;
//	}
//}

// Finds the chunk corresponding to a pointer, using an efficient search
Chunk* FixedAllocator::VicinityFind(void* p)
{
	assert(!chunks_.empty());
	assert(deallocChunk_);

	const std::size_t chunkLength = numBlocks_ * blockSize_;

	Chunk* lo = deallocChunk_;
	Chunk* hi = deallocChunk_ + 1;
	Chunk* loBound = &chunks_.front();
	Chunk* hiBound = &chunks_.back() + 1;

	//Special case: deallocChunk_ is the last in the array
	if (hi == hiBound) hi = nullptr;

	while (true)
	{
		if (lo)
		{
			if (p >= lo->pData_ && p < lo->pData_ + chunkLength)
			{
				return lo;
			}
			if (lo == loBound) lo = nullptr;
			else --lo;
		}

		if (hi)
		{
			if (p >= hi->pData_ && p < hi->pData_ + chunkLength)
			{
				return hi;
			}
			if (++hi == hiBound) hi = nullptr;
		}
	}

	assert(false);
	return 0;
}

// Performs deallocation. Assumes deallocChunk_ points to the correct chunk
void FixedAllocator::DoDeallocate(void* p)
{
	assert(deallocChunk_->pData_ <= p);
	assert(deallocChunk_->pData_ + numBlocks_ * blockSize_ > p);

	// call into the chunk, will adjust the inner list but won't release memory
	deallocChunk_->Deallocate(p, blockSize_);

	if (deallocChunk_->blocksAvailable_ == numBlocks_)
	{
		// deallocChunk_ is completely free, should we release it?

		Chunk& lastChunk = chunks_.back();
		if (&lastChunk == deallocChunk_)
		{
			// check if we have two last chunks empty
			if (chunks_.size() > 1 && deallocChunk_[-1].blocksAvailable_ == numBlocks_)
			{
				// Two free chunks, discard the last one
				lastChunk.Release();
				chunks_.pop_back();
				allocChunk_ = deallocChunk_ = &chunks_.front();
			}
			return;
		}

		if (lastChunk.blocksAvailable_ == numBlocks_)
		{
			// Two free blocks, discard one
			lastChunk.Release();
			chunks_.pop_back();
			allocChunk_ = deallocChunk_;
		}
		else
		{
			// move the empty chunk to the end
			std::swap(*deallocChunk_, lastChunk);
			allocChunk_ = &chunks_.back();
		}
	}
}

std::size_t FixedAllocator::BlockSize() const
{
	return blockSize_;
}

bool FixedAllocator::operator<(std::size_t rhs) const
{
	return BlockSize() < rhs;
}