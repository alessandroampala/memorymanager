#pragma once
#include <cassert>
#include <chrono>
#include <iostream>
#include <vector>
#include "MemoryManager.h"
#include "operators_overloads.h"
using namespace std::chrono;

class Test
{
public:

	//in order for this to function work properly you have to disable the new operator overload
	void TestManyAllocationsSmallObj()
	{
		MemoryManager* const mm = MemoryManager::GetInstance();

		{
			auto start = high_resolution_clock::now();

			for (int i = 0; i < 10000; ++i)
			{
				mm->Allocate(1);
			}

			auto stop = high_resolution_clock::now();
			auto duration = duration_cast<microseconds>(stop - start);

			std::cout << "10000 allocations of 1 byte with MemoryManager: " << duration.count() << " microseconds" << std::endl;
		}

		{
			auto start = high_resolution_clock::now();

			for (int i = 0; i < 10000; ++i)
			{
				new char;
			}

			auto stop = high_resolution_clock::now();
			auto duration = duration_cast<microseconds>(stop - start);

			std::cout << "10000 allocations of 1 byte with standard new: " << duration.count() << " microseconds" << std::endl;
		}
	}

	//in order for this function to work properly you have to disable the new operator overload
	void TestManyAllocationsBigObj()
	{
		MemoryManager* const mm = MemoryManager::GetInstance();

		{
			auto start = high_resolution_clock::now();

			for (int i = 0; i < 10000; ++i)
			{
				mm->Allocate(4096 * 4);
			}

			auto stop = high_resolution_clock::now();
			auto duration = duration_cast<microseconds>(stop - start);

			std::cout << "10000 allocations of 16.384 byte with MemoryManager: " << duration.count() << " microseconds" << std::endl;
		}

		{
			auto start = high_resolution_clock::now();

			for (int i = 0; i < 10000; ++i)
			{
				new char[4096 * 4];
			}

			auto stop = high_resolution_clock::now();
			auto duration = duration_cast<microseconds>(stop - start);

			std::cout << "10000 allocations of 16.384 byte with standard new: " << duration.count() << " microseconds" << std::endl;
		}
	}

	static void TestAllocateInt()
	{
		//small objects
		MemoryManager* const mm = MemoryManager::GetInstance();
		int* num = static_cast<int*>(mm->Allocate(sizeof(int))); //allocation
		*num = 4; //memory access

		assert(mm->GetStats().allocatedBytes == sizeof(int) &&
			mm->GetStats().allocationsNum == 1);

		mm->Delete(num, sizeof(int));

		assert(mm->GetStats().allocatedBytes == 0 &&
			mm->GetStats().allocationsNum == 0);
	}

	static void TestAllocateNewIntArray()
	{
		MemoryManager* const mm = MemoryManager::GetInstance();
		int* num_array = new int[1024]; //max dim for the SmallObjAllocator (4096 = 1024 * sizeof(int))
		num_array[0] = 5;

		assert(mm->GetStats().allocatedBytes == sizeof(int) * 1024 &&
			mm->GetStats().allocationsNum == 1);

		/*
		delete FAILS because compiler doesn't link the right sized ::operator delete
		delete[] num_array;*/

		mm->GetInstance()->DeleteArray(num_array, sizeof(int) * 1024);

		assert(mm->GetStats().allocatedBytes == 0 &&
			mm->GetStats().allocationsNum == 0);

	}

	static void TestBigAllocation()
	{
		MemoryManager* const mm = MemoryManager::GetInstance();
		int* num_array = new int[4096]; 

		assert(mm->GetStats().allocatedBytes == sizeof(int) * 4096 &&
			mm->GetStats().allocationsNum == 1);

		mm->GetInstance()->DeleteArray(num_array, sizeof(int) * 4096);

		assert(mm->GetStats().allocatedBytes == 0 &&
			mm->GetStats().allocationsNum == 0);
	}

	static void TestSTLContainer()
	{
		MemoryManager* const mm = MemoryManager::GetInstance();
		std::vector<int, MMAllocator<int>> v(10);

		for (int i = 0; i < 10; ++i)
		{
			v[i] = i;
		}

		assert(mm->GetStats().allocatedBytes == 10 + 1 &&
			mm->GetStats().allocationsNum == 2);
	}

	static void RunTests()
	{
		TestAllocateInt();
		TestAllocateNewIntArray();
		TestBigAllocation();
		TestSTLContainer();
	}

};