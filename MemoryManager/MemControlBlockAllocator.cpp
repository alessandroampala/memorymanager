#include <cassert>

#include "MemControlBlockAllocator.h"

MemControlBlockAllocator::MemControlBlock::MemControlBlock(bool available, MemControlBlock* prev, MemControlBlock* next) :
	available_(available), prev_(prev), next_(next)
{
}

void MemControlBlockAllocator::MemControlBlock::MergeWithNextBlock()
{
	MemControlBlock* toDestroy = this->next_;
	this->next_ = this->next_->next_;
	toDestroy->~MemControlBlock();
}

void* MemControlBlockAllocator::Allocate(std::size_t numBytes)
{
	//is a suitable pool present?
	void* data = nullptr;
	for (int i = 0; i < pools.size(); ++i)
	{
		void* data = AllocateInPool(pools[i], numBytes);
		if (data != nullptr)
			return data;
	}

	//get available size as firstBlock + POOL_SIZE - thisBlock >= numBytes ?
	//the first MemControlBlock always has prev_ = nullptr
	//the last MemControlBlock always has next_ = nullptr

	//There aren't existing suitable pools, so create one
	//Special case, create ad hoc pool
	if (POOL_SIZE < numBytes + sizeof(MemControlBlock))
	{
		void* poolMemory = CreatePool(numBytes);
		MemControlBlock* firstBlock = static_cast<MemControlBlock*>(poolMemory);
		pools.push_back(firstBlock);
		return static_cast<char*>(poolMemory) + sizeof(MemControlBlock);
	}

	//create a normal pool
	void* poolMemory = CreatePool(POOL_SIZE - sizeof(MemControlBlock));
	MemControlBlock* firstBlock = static_cast<MemControlBlock*>(poolMemory);
	pools.push_back(firstBlock);
	return AllocateInPool(firstBlock, numBytes);
}

void MemControlBlockAllocator::Deallocate(void* p)
{
	MemControlBlock* block = static_cast<MemControlBlock*>(static_cast<void*>((static_cast<char*>(p) - sizeof(MemControlBlock))));
	block->available_ = true;
	
	if (block->next_ && block->next_->available_)
	{
		block->MergeWithNextBlock();
	}
	if (block->prev_ && block->prev_->available_)
	{
		MemControlBlock* prev = block->prev_;
		prev->MergeWithNextBlock();
		block = prev;
	}

	//if the entire pool is free and pool size == POOL_SIZE and pools.size() > 1
	//release memory of this pool
	
	//if the entire pool is free release the memory
	if (IsPoolFree(block))
	{
		free(static_cast<void*>(block));
		std::vector<MemControlBlock*>::iterator it;
		//WARNING: Linear search in pools!
		it = find(pools.begin(), pools.end(), block);
		if (it == pools.end()) //element not found
			assert(false);

		pools.erase(it);
	}
}

void* MemControlBlockAllocator::CreatePool(std::size_t numBytes)
{
	void* poolMemory = malloc(sizeof(MemControlBlock) + numBytes);
	//construct first block (only construction, no allocation here)
	new(poolMemory) MemControlBlock(true, nullptr, nullptr);
	return poolMemory;
}

//this has to be called only in case of normal allocation. won't work with ad hoc pools
void* MemControlBlockAllocator::AllocateInPool(MemControlBlock* head, std::size_t numBytes)
{
	MemControlBlock* block = head;

	while (block)
	{
		if (block->available_)
		{
			std::size_t blockBytes;
			if (block->next_)
			{
				blockBytes = static_cast<char*>(static_cast<void*>(block->next_)) - (static_cast<char*>(static_cast<void*>(block)) + sizeof(MemControlBlock));
			}
			else //block->next == nullptr
			{
				blockBytes = static_cast<char*>(static_cast<void*>(head)) + POOL_SIZE - ((static_cast<char*>(static_cast<void*>(block)) + sizeof(MemControlBlock)));
			}

			if (blockBytes >= numBytes)
			{
				block->available_ = false;
				void* data = static_cast<char*>(static_cast<void*>(block)) + sizeof(MemControlBlock);

				if (blockBytes > numBytes + sizeof(MemControlBlock)) //create a new control block
				{
					MemControlBlock* newBlock = static_cast<MemControlBlock*>(static_cast<void*>((static_cast<char*>(data) + numBytes)));
					new(newBlock) MemControlBlock(true, block, block->next_);
					block->next_ = static_cast<MemControlBlock*>(newBlock);
					if (newBlock->next_)
					{
						newBlock->next_->prev_ = newBlock;
					}
				}

				return data;
			}
		}
		block = block->next_;
	}

	return nullptr;
}

bool MemControlBlockAllocator::IsPoolFree(MemControlBlock* head)
{
	return head != nullptr && head->available_ && head->prev_ == nullptr && head->next_ == nullptr;
}
