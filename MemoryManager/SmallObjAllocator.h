#pragma once

#include <cstddef>
#include <vector>
#include "FixedAllocator.h"
#include "HeapAllocator.h"

class SmallObjAllocator
{
public:
	SmallObjAllocator(std::size_t chunkSize, std::size_t maxObjectSize);
	void* Allocate(std::size_t numBytes);
	void Deallocate(void* p, std::size_t numBytes);


private:
	std::vector<FixedAllocator, HeapAllocator<FixedAllocator>> pool_;
	FixedAllocator* pLastAlloc_;
	FixedAllocator* pLastDealloc_;
	std::size_t chunkSize_;
	std::size_t maxObjectSize_;
};
