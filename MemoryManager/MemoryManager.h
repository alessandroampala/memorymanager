#pragma once

#include <cstdint>
#include "SmallObjAllocator.h"
#include "MemControlBlockAllocator.h"

/*
* USEFUL MACROS:
* - MM_DISABLE_OVERLOADS: disables global overloads of 
*						new, new[], delete, delete[] operators
*/

struct MMStats
{
	uint64_t allocationsNum = 0;
	uint64_t allocatedBytes = 0;
};

class MemoryManager
{
public:

	bool verbose = false;

	MemoryManager();
	MemoryManager(int smallObjByteThreshold);
	virtual ~MemoryManager();

	void* Allocate(std::size_t numBytes);
	void* AllocateArray(std::size_t numBytes);
	void Delete(void* p, std::size_t numBytes);
	void DeleteArray(void* p, std::size_t numBytes);

	const MMStats& GetStats() const;
	static MemoryManager* const GetInstance();
	void SetVerbosity(bool verbose);

private:
	const int smallObjByteThreshold = 4096;
	const std::size_t smallObjAllocatorChunkSize = 512;

	//allocators
	SmallObjAllocator smallObjAllocator;
	MemControlBlockAllocator mcbAllocator;

	//singleton
	static MemoryManager* instance;
	//statistics
	MMStats stats;
};
