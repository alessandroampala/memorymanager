#include <iostream>
#include <cassert>
#include "MemoryManager.h"

MemoryManager::MemoryManager(int smallObjByteThreshold) :
	smallObjByteThreshold(smallObjByteThreshold),
	smallObjAllocator(smallObjAllocatorChunkSize, smallObjByteThreshold)
{

}

MemoryManager::MemoryManager() :
	smallObjAllocator(smallObjAllocatorChunkSize, smallObjByteThreshold)
{

}

MemoryManager::~MemoryManager()
{
	//memory leak detection
	if (stats.allocationsNum > 0 || stats.allocatedBytes > 0)
	{
		std::cout << "MEMORY LEAK DETECTED: " << stats.allocationsNum << " allocations ("
			<< stats.allocatedBytes << " bytes) have not been deallocated." << std::endl;
		assert(false);
	}
}

void* MemoryManager::Allocate(std::size_t numBytes)
{
	++stats.allocationsNum;
	stats.allocatedBytes += numBytes;

	void* address = numBytes <= smallObjByteThreshold ?
		smallObjAllocator.Allocate(numBytes) : mcbAllocator.Allocate(numBytes);

	if (verbose)
		std::cout << "Allocated: " << numBytes << " bytes, address: " << address << std::endl;
	return address;
}

void* MemoryManager::AllocateArray(std::size_t numBytes)
{
	return Allocate(numBytes);
}

void MemoryManager::Delete(void* p, std::size_t numBytes)
{
	--stats.allocationsNum;
	stats.allocatedBytes -= numBytes;

	if (numBytes <= smallObjByteThreshold)
		smallObjAllocator.Deallocate(p, numBytes);
	else
		mcbAllocator.Deallocate(p);

	if (verbose)
		std::cout << "Deallocated: " << numBytes << " bytes, address: " << p << std::endl;
}

void MemoryManager::DeleteArray(void* p, std::size_t numBytes)
{
	Delete(p, numBytes);
}

const MMStats& MemoryManager::GetStats() const
{
	return stats;
}

MemoryManager* const MemoryManager::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new(malloc(sizeof(MemoryManager))) MemoryManager;
	}
	return instance;
}

void MemoryManager::SetVerbosity(bool verbose)
{
	this->verbose = verbose;
}

MemoryManager* MemoryManager::instance;
