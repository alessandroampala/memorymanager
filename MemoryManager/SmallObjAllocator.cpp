#include <cassert>
#include <iostream>
#include "SmallObjAllocator.h"

// Used by std::lower_bound in SmallObjAllocator
static bool operator <(const FixedAllocator& lhs, const FixedAllocator& rhs)
{
	return lhs.BlockSize() < rhs.BlockSize();
}

SmallObjAllocator::SmallObjAllocator(std::size_t chunkSize, std::size_t maxObjectSize) :
	pLastAlloc_(nullptr),
	pLastDealloc_(nullptr),
	chunkSize_(chunkSize),
	maxObjectSize_(maxObjectSize)
{
}

void* SmallObjAllocator::Allocate(std::size_t numBytes)
{
	if (numBytes > maxObjectSize_) return operator new(numBytes);

	if (pLastDealloc_ && pLastDealloc_->BlockSize() == numBytes)
	{
		void* allocatedAddress = pLastAlloc_->Allocate();
		return allocatedAddress;
	}

	//binary search of FixedAllocator of requested BlockSize
	FixedAllocator aux(numBytes);
	std::vector<FixedAllocator>::iterator i = std::lower_bound(pool_.begin(), pool_.end(), aux);
	if (i == pool_.end() || i->BlockSize() != numBytes)
	{
		i = pool_.insert(i, FixedAllocator(numBytes));
		pLastDealloc_ = &*pool_.begin();
	}
	pLastAlloc_ = &*i;
	
	void* allocatedAddress = pLastAlloc_->Allocate();
	return allocatedAddress;
}

void SmallObjAllocator::Deallocate(void* p, std::size_t numBytes)
{
	if (numBytes > maxObjectSize_) return operator delete(p);
	
	if (pLastDealloc_ && pLastDealloc_->BlockSize() == numBytes)
	{
		pLastDealloc_->Deallocate(p);
		return;
	}

	FixedAllocator aux(numBytes);
	std::vector<FixedAllocator>::iterator i = std::lower_bound(pool_.begin(), pool_.end(), aux);
	assert(i != pool_.end());
	assert(i->BlockSize() == numBytes);
	pLastDealloc_ = &*i;
	pLastDealloc_->Deallocate(p);
}
