#include <iostream>
#include "MemoryManager.h"
#include "MMAllocator.h"
//#define MM_DISABLE_OVERLOADS
#include "operators_overloads.h"
#include "Tests.h"


int main()
{
    MemoryManager::GetInstance()->SetVerbosity(true);
    Test::RunTests();
    std::cout << "All tests passed!" << std::endl;
}