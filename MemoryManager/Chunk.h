#pragma once
#include <cstddef>

#ifndef DEFAULT_CHUNK_SIZE
#define DEFAULT_CHUNK_SIZE 4096
#endif

// Nothing is private � Chunk is a Plain Old Data (POD) structure
// structure defined inside FixedAllocator
// and manipulated only by it
struct Chunk
{
	void Init(std::size_t blockSize, unsigned char blocks);
	void* Allocate(std::size_t blockSize);
	void Deallocate(void* p, std::size_t blockSize);
	void Release();
	unsigned char* pData_;
	unsigned char
		firstAvailableBlock_, //holds the index of the first block available in this chunk
		blocksAvailable_; //the number of blocks available in this chunk
};

