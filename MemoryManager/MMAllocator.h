#pragma once

#include <cstddef>
#include "MemoryManager.h"

template<typename T>
class MMAllocator
{
public:

	typedef T value_type;

	MMAllocator() noexcept {}
	template<class U> MMAllocator(const MMAllocator<U>&) noexcept {}

	inline T* allocate(std::size_t n)
	{
		return static_cast<T*>(MemoryManager::GetInstance()->Allocate(n));
	}

	inline void deallocate(T* p, std::size_t n)
	{
		return MemoryManager::GetInstance()->Delete(p, n);
	}
};
