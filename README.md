# Memory Manager
Custom Memory Manager that uses Small Object and MemControlBlock allocators.

This allocator is **implemented using two different underlying allocators**.
The first one is **Andrei Alexandrescu's Small Object Allocator**, useful for allocating objects of small size in order to have faster small allocations.
The second one is implemented using **MemControlBlocks** that keeps track of the memory as a doubly linked list.

The Memory Manager automatically delegates allocation requests to the most suitable allocator.

### Singleton
The MemoryManager lazily instances a singleton which you can access with by `MemoryManager::GetInstance()`.
Most of the time you won't need to allocate directly using the instance since the overloads of new and delete are present, but it is useful if you want to change the settings or get the stats of the MemoryManager.
Of course you can create your own MemoryManager object passing a custom `smallObjByteThreshold` in the constructor.

### Overload of new and delete operators
It is possible to use the overloaded global operators 
- `::operator new`
- `::operator new[]`
- `::operator delete`
- `::operator delete[]`

that use MemoryManager to dispatch allocation requests.
In order to do so, you need to `#include "operators_overloads.h"` and they will be enabled by default.
It is also possible to disable them with `#define MM_DISABLE_OVERLOADS` before the file inclusion.

### std::allocator compatibility
It is possible to use the MemoryManager allocator **with STL containers**, passing `MMAllocator<T>` as the allocator parameter of the template:

	std::vector<int, MMAllocator<int>> v;

### Verbosity
It is possible to enable the debug prints whenever an allocation or deallocation occurs by calling `SetVerbosity(true)`. In that case, the output will be something like

	Allocated: 20000 bytes, address: 000001FB45EC49C8

### Stats
The MemoryManager **keeps track of useful stats** which you can access to in order to know what's going on calling `GetStats()` on the MemoryManager object.

### MemoryManager vs standard new

The comparison was made performing 10000. The results are shown below

	10000 allocations of 1 byte with MemoryManager: 80 microseconds
	10000 allocations of 1 byte with standard new: 410 microseconds
	10000 allocations of 16.384 byte with MemoryManager: 210721 microseconds
	10000 allocations of 16.384 byte with standard new: 10934 microseconds

This results in a **speed improvement of 80.5%** in case of small objects, but on the other hand is evident that the MemControlBlock allocator behaves really bad compared to the standard one.